package com.unclebae.thiss.iss.aops.aspect;

import org.aspectj.lang.ProceedingJoinPoint;

public class LoggingAspect {

	public void beforeAdvice() {
		System.out.println("------------------------ Before Advice");
	}
	public void afterAdvice() {
		System.out.println("======================== After Advice");
	}
	public void afterReturningAdvice(Object retVal) {
		System.out.println("-.-.-.-.-.-.-.-.-.-.-.- After Returning Advice : " + retVal);
	}
	public void afterThrowingAdvice(Exception ex) {
		System.out.println("=.=.=.=.=.=.=.=.=.=.=.=.= After Throwing Advice : " + ex.getMessage());
	}
	public void aroundAdvice(ProceedingJoinPoint jp) throws Throwable {
		System.out.println(">>>> Start Around Advice");
		Object[] arguments = jp.getArgs();
		if (arguments != null && arguments.length > 0) {
			for (int i = 0; i < arguments.length; i++) {
				System.out.println(String.format("Argument [%s][%s] ", i, arguments[i]));
			}
		}
		
		Object result = jp.proceed(arguments);
		
		System.out.println(">>>> End Around Advice");
	}
}

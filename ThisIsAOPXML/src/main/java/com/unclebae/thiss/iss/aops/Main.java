package com.unclebae.thiss.iss.aops;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.unclebae.thiss.iss.aops.entity.TargetObject;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("springcontext.xml");
		TargetObject target = (TargetObject) context.getBean("targetObject");
		
		target.printAge();
		System.out.println();
		
		target.printName();
		System.out.println();
		
		target.printReturnMethod("kido", "kido2", "kido3");
		System.out.println();
		
		target.printException();
	}
}
